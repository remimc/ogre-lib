#!/bin/env python
#*-* codiing: utf-8 *-*

from lib.globals import config
from lib.manager.managerFactory import ManagerFactory

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import OperationalError
from sqlalchemy.exc import ProgrammingError

from lib.exceptions import DBError
from lib.exceptions import BuildingError
from lib.exceptions import SwitchError
from lib.exceptions import RoomError
from lib.exceptions import MemberError
from lib.exceptions import MachineError
from lib.exceptions import IpError
from lib.exceptions import OrganizationError
from lib.exceptions import PaymentError

from lib.entity.organization import Organization

class AppFacade(object):
	"""
	"""
	instance = None       
	def __new__(cls, *args, **kargs): 
		if cls.instance is None:
			cls.instance = object.__new__(cls, *args, **kargs)
		return cls.instance
	
	def __init__(self):
		"""
		"""
		dburi = config.get("database", "uri")
		
		engine = create_engine(dburi, echo=True, poolclass=NullPool)
		Session = sessionmaker(bind=engine)
		
		self.db_session = Session()
		self.factory = ManagerFactory(self.db_session)
		
		self.buildings = self.factory.buildingManager()
		self.switchs = self.factory.switchManager()
		self.rooms = self.factory.roomManager()
		self.members = self.factory.memberManager()
		self.machines = self.factory.machineManager()
		self.ip = self.factory.ipManager()
		self.payment = self.factory.paymentManager()
		self.organization = self.factory.organizationManager()
		
		try:
			self.payment_types = self.factory.paymentTypeManager().find_all()
			self.organization_roles = self.factory.organizationRoleManager().find_all()
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Init facade : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Init facade : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Init facade : %s" % e)
		except Exception as e:
			raise e
	##
	## Acces to static data
	##
	
	def find_building(self, value):
		"""
		"""
		try:
			return self.buildings.find(value)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find building : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find building : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find building : %s" % e)
		except Exception as e:
			raise BuildingError("Unable to find building '%s' : %s" % (value, e))
	
	def find_all_buildings(self, begin=None, end=None):
		"""
		"""
		try:
			return self.buildings.find_all(begin, end)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find all buildings : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find all buildings : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find all buildings : %s" % e)
		except Exception as e:
			raise BuildingError("Can't find buildings : %s" % e)
		
	def find_switch(self, value, find_by='name'):
		"""
		"""
		try:
			return self.switchs.find(value, find_by)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find switch : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find switch : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find switch : %s" % e)
		except Exception as e:
			raise SwitchError("Can't find switch for '%s' = '%s' : %s" % (find_by, value, e)) 
		
	def find_all_switchs(self, order_by='name', rev=False, start=None, stop=None):
		"""
		"""
		try:
			return self.switchs.find_all(order_by, rev, begin, end)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find all switchs : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find all switchs : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find all switchs : %s" % e)
		except Exception as e:
			raise SwitchError("Unable to find switchs : %s" % e)
	
	def find_room(self, value, find_by='name'):
		"""
		"""
		try:
			return self.rooms.find(value, find_by)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find room : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find room : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find room : %s" % e)
		except Exception as e:
			raise RoomError("Can't find room for '%s' = '%s' : %s" % (find_by, value, e))
		
	def find_all_rooms(self, order_by='name', rev=False, start=None, stop=None):
		"""
		"""
		try:
			return self.rooms.find_all(order_by, rev, start, stop)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find all rooms : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find all rooms : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find all rooms : %s" % e)
		except Exception as e:
			raise RoomError("Can't find rooms : %s" % e)
		
	def find_free_rooms(self):
		"""
		"""
		try:
			return self.rooms.find_free()
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find free rooms : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find free rooms : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find free rooms : %s" % e)
		except Exception as e:
			raise RoomError("Can't find free rooms : %s" % e)
	
	##
	## Manage members
	##
	
	# Members
	
	def search_member(self, value, search_by='id'):
		"""
		"""
		try:
			return self.members.search(value, search_by)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Search member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Search member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Search member : %s" % e)
		except Exception as e:
			raise MemberError("Can't find any member for '%s' like '%s' : %s" % (search_by, value, e))
		
	def find_member(self, value, find_by='id'):
		"""
		"""
		try:
			return self.members.find(value, find_by)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find member : %s" % e)
		except Exception as e:
			raise MemberError("Can't find member for '%s' = '%s' : %s" % (find_by, value, e))
			
	def find_all_members(self, order_by='id', rev=False, start=None, stop = None):
		"""
		"""
		try:
			return self.members.find_all(order_by, rev, start, stop)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find all members : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find all members : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find all members : %s" % e)
		except Exception as e:
			raise MemberError("Can't find members : %s" % e)
		
	def add_member(self, member):
		"""
		"""
		try:
			return self.members.add(member)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Add member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Add member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Add member : %s" % e)
		except Exception as e:
			raise MemberError("Can't add member '%s' : %s" % (member, e))
		
	def update_member(self, member):
		"""
		"""
		try:
			return self.members.update(member)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Update member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Update member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Update member : %s" % e)
		except Exception as e:
			raise MemberError("Can't update member '%s' : %s" % (member, e))
		
	def delete_member(self, member):
		"""
		"""
		try:
			return self.members.delete(member)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Delete member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Delete member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Delete member : %s" % e)
		except Exception as e:
			raise MemberError("Can't delete member '%s' : %s" % (member, e))
		
	def find_expired_members(self):
		"""
		"""
		try:
			return self.members.get_expired()
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find expired members : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find expired members : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find expired members : %s" % e)
		except Exception as e:
			raise MemberError("Can't find expired members : %s" % e)
		
	def get_number_of_members(self):
		"""
		"""
		try:
			return self.members.count()
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Count members : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Count members : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Count members : %s" % e)
		except Exception as e:
			raise MemberError("Can't count members : %s" % e)
		
	def find_machines_for_member(self, member):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, find machines" % str(member.id))
		
			machines = list()
			for machine in self.machines.find(member.id, find_by='owner'):
				ip = self.ip.find(machine.mac, find_by='mac')
				if ip:
					machines.append((machine, ip[0].ip))
				else:
					machines.append((machine, None))
			return machines
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find machines for member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find machines for member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find machines for member : %s" % e)
		except Exception as e:
			raise MemberError("Can't find machines for member '%s' : %s" % (member, e))
		
	
	def add_machine_to_member(self, machine, member):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			if not machine.owner:
				machine.owner = member.id
			
			# Check if mac address already exists
			if machine.mac:
				if self.machines.find(machine.mac, find_by='mac'):
					# Mac address already exists, cannot add machine
					raise MachineError("Machine MAC address already exists")
				else:
					# Mac address doesn't exists, can add machine
					self.machines.add(machine)
					
					# Add an ip address for the new mac address
					self.ip.add_mac(machine.mac)
			else:
				raise MachineError("Cannot add machine without a MAC address")
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Add machine to member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Add machine to member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Add machine to member : %s" % e)
		except Exception as e:
			raise MemberError("Can't add machine '%s' to member '%s' : %s" % (machine, member, e))
		
	# payments 
	
	def add_payment_to_member(self, payment, member):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			payment.member = member.id
			self.payment.add(payment)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Add payment to member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Add payment to member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Add payment to member : %s" % e)
		except Exception as e:
			raise PaymentError("Can't add payment '%s' to member '%s' : %s" % (payment, member, e)) 
	
	def find_payments_for_member(self, member):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			return self.payment.find(member.id, find_by='member')
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find payments for member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find payments for member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find payments for member : %s" % e)
		except Exception as e:
			raise PaymentError("Can't find mayments for member '%s' : %s" % (member, e))
	
	def find_payment(self, value, find_by='id'):
		"""
		"""
		try:
			return self.payment.find(value, find_by)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find payment : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find payment : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find payment : %s" % e)
		except Exception as e:
			raise PaymentError("Can't find mayment '%s' = '%s' : %s" % (find_by, value, e))
		
	def find_all_payments(self, order_by='id', rev=False, start=None, stop=None):
		"""
		"""
		try:
			return self.payment.find_all(order_by, rev, start, stop)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find all payments : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find all payments : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find all payments : %s" % e)
		except Exception as e:
			raise PaymentError("Can't find payements : %s" % e)
		
	def update_payment(self, payment):
		"""
		"""
		try:
			self.payment.update(payment)
			
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Update payment : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Update payment : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Update payment : %s" % e)
		except Exception as e:
			raise PaymentError("Can't update payment '%s' : %s" % (payment, e))
		
	def delete_payment(self, payment):
		"""
		"""
		try:
			self.payment.delete(payment)
			
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Delete payment : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Delete payment : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Delete payment : %s" % e)
		except Exception as e:
			raise PaymentError("Can't delete payment '%s' : %s" % (payment, e))
		
	def find_payment_types(self):
		"""
		"""
		try:
			return self.payment_types
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find payment types : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find payment types : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find payment types : %s" % e)
		except Exception as e:
			raise PaymentError("Can't find payment types: %s" % e)
		
	# Organization
	
	def find_all_organization(self, order_by="id", rev=False, start=None, stop=None):
		"""
		"""
		try:
			return self.organization(order_by, rev, start, stop)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find all organization : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find all organization  : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find all organization  : %s" % e)
		except Exception as e:
			raise OrganizationError("Can't find organization roles for members : %s" % e)
	
	def find_roles_for_member(self, member):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			return [r.role for r in self.organization.find(member.id, find_by='member')]
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find role for member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find role for member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find role for member : %s" % e)
		except Exception as e:
			raise OrganizationError("Can't find roles for member '%s' : %s" % (member, e))
	
	def add_role_to_member(self, member, role):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			self.organization.add(Organization(role=role, member=member.id))
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Add role to member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Add role to member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Add role to member : %s" % e)
		except Exception as e:
			raise OrganizationError("Can't add role '%s' to member '%s' : %s" % (role, member, e))
		
	def remove_role_from_member(self, member, role):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			member_roles = self.organization.find(membre.id, find_by='member')
			if not member_roles:
				raise OrganizationError("Member '%s' has no organization role" % member.id)
			
			for org in member_roles:
				if org.role == role:
					self.organization.delete(org)
					return
			raise OrganizationError("Role not found for member '%s'" % member.id)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Remove role from member : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Remove role from member : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Remove role from member : %s" % e)
		except Exception as e:
			raise OrganizationError("Can't remove role '%s' from member '%s': %s" % (role, member, e))
		
	def remove_member_from_organization(self, member):
		"""
		"""
		try:
			# Check if member exists
			if not self.members.find(member.id, find_by='id'):
				# Member doesn't exists, can't add machine
				raise MemberError("Member %s doesn't exists, can't add machine" % str(member.id))
			
			member_roles = self.organization.find(membre.id, find_by='member')
			if not member_roles:
				raise OrganizationError("Member '%s' has no organization role" % member.id)
			
			for org in member_roles:
				self.organization.delete(org)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Remove member organization : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Remove member organization : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Remove member organization : %s" % e)
		except Exception as e:
			raise OrganizationError("Can't remove member '%s' from organization : %s" % (member, e))
		
	def find_organization_roles(self):
		"""
		"""
		try:
			return self.organization_roles
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find organization roles : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find organization roles : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find organization roles : %s" % e)
		except Exception as e:
			raise OrganizationError("Can't fins organization roles : %s" % e)
		
	# machines
	
	def find_machine(self, value, find_by='mac'):
		"""
		"""
		try:
			if find_by == 'ip':
				ip = self.ip.find(value, find_by='ip')
				if not ip:
					raise IpError("Can't find machine for ip '%s'" % (value))
				value = ip[0].mac
				find_by = 'mac'
			machine = self.machines.find(value, find_by)[0]
			return (machine, self.ip.find(machine.mac, find_by='mac'))
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Find machine : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Find machine : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Find machine : %s" % e)
		except Exception as e:
			raise MachineError("Can't find machine '%s' = '%s' : %s" % (find_by, value, e))
		
	def update_machine(self, machine):
		"""
		"""
		try:
			if not machine.mac:
				# Can't remove machine without MAC address
				raise MachineError("Can't remove machine without MAC address")
			self.machines.add(machine)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Update machine : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Update machine : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Update machine : %s" % e)
		except Exception as e:
			raise MachineError("Can't update machine '%s' : %s" % (machine, e))
		
	def remove_machine(self, machine):
		"""
		"""
		try:
			if not machine.mac:
				# Can't remove machine without MAC address
				raise MachineError("Can't remove machine without MAC address")
		
			self.machines.delete(machine)
		
		except ProgrammingError as e:
			raise DBError(DBError.PROGRAMMING, "Remove machine : %s" % e)
		except IntegrityError as e:
			raise DBError(DBError.INTEGRITY, "Remove machine : %s" % e)
		except OperationalError as e:
			raise DBError(DBError.OPERATIONAL, "Remove machine : %s" % e)
		except Exception as e:
			raise MachineError("Can't delete machine '%s' : %s" % (machine, e))
		
