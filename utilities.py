#!/bin/env python
#*-* coding: utf-8 *-*

import re
import unicodedata
from datetime import datetime,timedelta
from calendar import monthrange

def remove_accents(input_str):
	if not isinstance(input_str, unicode):
		input_str = input_str.decode('utf-8')
	nkfd_form = unicodedata.normalize('NFKD', input_str)
	only_ascii = nkfd_form.encode('ASCII', 'ignore')
	return only_ascii

def check_mac(mac):
	if re.match(r'([a-fA-F0-9]{2}[ :wq:-]){5}[a-fA-F0-9]{2}', mac):
		return ':'.join(re.split(r'[ :-]', mac)).lower()
	elif re.match(r'[a-fA-F0-9]{12}', mac):
		return ':'.join(re.findall('..', mac)).lower()
	else:
		return None

def months_between(start,end):
	months = -1
	cursor = start
	while cur <= end:
		cur += timedelta(days=monthrange(cur.year, cur.month)[1])
		months += 1
	return months

def month_add(date, months):
    for i in range(months):
        date += timedelta(days=monthrange(date.year, date.month)[1])
    return date