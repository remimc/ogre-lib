#!/bin/env python
#*-* codiing: utf-8 *-*

from sqlalchemy import Table, MetaData, Column, ForeignKey, Integer, String, Boolean, Date, Numeric, Text
from sqlalchemy.orm import mapper

from lib.entity.building import Building
from lib.entity.switch import Switch
from lib.entity.room import Room
from lib.entity.member import Member
from lib.entity.machine import Machine
from lib.entity.ip import Ip
from lib.entity.organization import Organization, OrganizationRole
from lib.entity.payment import Payment, PaymentType

metadata = MetaData()

# Declare database mappings

building = Table('buildings', metadata,
	Column('name', String(45), primary_key=True))

switch = Table('switchs', metadata,
	Column('name', String(45), primary_key=True),
	Column('location', String(45), ForeignKey('buildings.name')),
	Column('address', String(15)))

room = Table('rooms', metadata,
	Column('name', String(45), primary_key=True),
	Column('floor', String(1)),
	Column('location', String(45), ForeignKey('buildings.name')),
	Column('switch', String(45), ForeignKey('switchs.name')),
	Column('port', Integer))

member = Table('members', metadata,
	Column('id', String(36), primary_key=True),
	Column('firstname', String(45)),
	Column('lastname', String(45)),
	Column('email', String(45)),
	Column('newsok', Boolean),
	Column('login', String(45)),
	Column('password', String(45)),
	Column('room', String(45), ForeignKey('rooms.name')),
	Column('regfrom', Date),
	Column('regvalid', Boolean),
	Column('networkuntil', Date),
	Column('blocked', Boolean))
		
machine = Table('machines', metadata,
	Column('mac', String(17), primary_key=True),
	Column('owner', String(36), ForeignKey("members.id")),
	Column('name', String(45)))

ippool = Table('ippool', metadata,
	Column('ip', String(15), primary_key=True),
	Column('mac', String(17), ForeignKey('machines.mac')))

organization = Table('organization', metadata,
	Column('id', Integer, primary_key=True), 
	Column('role', String(20), ForeignKey('organization_roles.role')),
	Column('member', String(36), ForeignKey('members.id')))

organization_role = Table('organization_roles', metadata,
	Column('id', Integer, primary_key=True),
	Column('role', String(20)))

payment = Table('payments', metadata,
	Column('id', Integer, primary_key=True),
	Column('date', Date),
	Column('amount', Numeric(7, 3)),
	Column('type', String(20), ForeignKey('payment_types.type')),
	Column('comment', Text),
	Column('member', String(36)))

payment_type = Table('payment_types', metadata,
	Column('id', Integer, primary_key=True),
	Column('type', String(20)))

# Map Python classes to database

mapper(Building, building)
mapper(Switch, switch)
mapper(Room, room)
mapper(Member, member)
mapper(Machine, machine)
mapper(Ip, ippool)
mapper(Organization, organization)
mapper(OrganizationRole, organization_role)
mapper(Payment, payment)
mapper(PaymentType, payment_type)