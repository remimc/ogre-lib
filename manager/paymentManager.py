#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.payment import Payment
from lib.entity.payment import PaymentType
from lib.exceptions import DBError

class PaymentManager(object):
	"""Implementation of the DAO pattern for the machine entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
	
	def find(self, value, find_by="id"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Payment)
		if find_by == "id":
			query = query.filter(Payment.id == value)
		elif find_by == "date":
			query = query.filter(Payment.date == value)
		elif find_by == "type":
			query = query.filter(Payment.type == value)
		elif find_by == "member":
			query = query.filter(Payment.member == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Payment" % find_by)
		return query.all()
	
	def find_all(self, order_by="id", rev=False, start=None, stop=None):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Payment)
		
		criterion = None
		if order_by == "id":
			criterion = Payment.id
		elif order_by == "date":
			criterion = Payment.date
		elif order_by == "type":
			criterion = Payment.type
		elif order_by == "member":
			criterion = Payment.member
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Payment" % order_by)
		if rev:
			query = query.order_by(desc(criterion))
		else:
			query = query.order_by(criterion)
		if start and not stop:
			query = query.limit(start)
		elif start and stop:
			query = query.slice(start, stop)
		
		return query.all()
	
	def add(self, payment):
		"""
		"""
		db = self.factory.get_connection()
		db.add(payment)
		db.commit()
	
	def update(self, payment):
		"""
		"""
		db = self.factory.get_connection()
		db.merge(payment)
		db.commit()
		
	def delete(self, payment):
		"""
		"""
		db = self.factory.get_connection()
		db.delete(payment)
		db.commit()
		
class PaymentTypeManager(object):
	"""
	"""
	
	def __init__(self, factory):
		self.factory = factory
		
	def find_all(self):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(PaymentType)
		 
		return [t.type for t in query.all()]