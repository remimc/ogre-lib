#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.ip import Ip
from lib.exceptions import DBError

class IpManager(object):
	"""Implementation of the DAO pattern for the Ip entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
		
	def find(self, value, find_by="ip"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Ip)
		if find_by == "mac":
			query = query.filter(Ip.mac == value)
		elif  find_by == "ip":
			query = query.filter(Ip.ip == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Machine" % find_by)
		return query.all()
	
	def update_mac(self, old_mac, new_mac):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Ip)
		
		ip = query.filter(Ip.mac == old_mac).one()
		ip.mac = new_mac
		db.merge(ip)
		db.commit()
	
	def add_mac(self, mac):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Ip)
		
		ip = query.filter(Ip.mac == None).first()
		ip.mac = mac
		db.merge(ip)
		db.commit()
		
	def delete_mac(self, mac):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Ip)
		
		ip = query.filter(Ip.mac == mac).one()
		ip.mac = None
		db.merge(ip)
		db.commit()