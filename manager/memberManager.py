#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.member import Member
from lib.exceptions import DBError
from datetime import date

class MemberManager(object):
	"""Implementation of the DAO pattern for the member entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
	
	def count(self):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Member)
		
		return query.count()
	
	def get_expired(self, end_date=None):
		"""
		"""
		if not end_date:
			end_date = date.today()
			
		db = self.factory.get_connection()
		query = db.query(Member)
		query = query.filter(Member.networkuntil < end_date)
		   
		return query.all()
	
	def find(self, value, find_by="id"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Member)
		if find_by == "id":
			query = query.filter(Member.id == value)
		elif  find_by == "firstname":
			query = query.filter(Member.firstname == value)
		elif  find_by == "lastname":
			query = query.filter(Member.lastname == value)
		elif  find_by == "email":
			query = query.filter(Member.email == value)
		elif  find_by == "login":
			query = query.filter(Member.login == value)
		elif  find_by == "password":
			query = query.filter(Member.password == value)
		elif  find_by == "room":
			query = query.filter(Member.room == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Member" % find_by)
		return query.all()
	
	def find_all(self, order_by="id", rev=False, start=None, stop = None):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Member)
		criterion = None
		if order_by == "id":
			criterion = Member.id
		elif  order_by == "firstname":
			criterion = Member.firstname
		elif  order_by == "lastname":
			criterion = Member.lastname
		elif  order_by == "email":
			criterion = Member.email
		elif  order_by == "login":
			criterion = Member.login
		elif  order_by == "password":
			criterion = Member.password
		elif  order_by == "room":
			criterion = Member.room
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Member" % order_by)
		if rev:
			query = query.order_by(desc(criterion))
		else:
			query = query.order_by(criterion)
		if start and not stop:
			query = query.limit(start)
		elif start and stop:
			query = query.slice(start, stop)
		return query.all()
	
	def search(self, value, search_by="id", start=None, stop=None):
		"""
		"""
		value = "%%%s%%" % value
		db = self.factory.get_connection()
		query = db.query(Member)
		if search_by == "id":
			query = query.filter(Member.id.like(value))
		elif  search_by == "firstname":
			query = query.filter(Member.firstname.like(value))
		elif  search_by == "lastname":
			query = query.filter(Member.lastname.like(value))
		elif  search_by == "email":
			query = query.filter(Member.email.like(value))
		elif  search_by == "login":
			query = query.filter(Member.login.like(value))
		elif  search_by == "password":
			query = query.filter(Member.password.like(value))
		elif  search_by == "room":
			query = query.filter(Member.room.like(value))
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Member" % search_by)
		if start and not stop:
				query = query.limit(start)
		elif start and stop:
				query = query.slice(start, stop)
		return query.all()
		
	
	def add(self, member):
		"""
		"""
		db = self.factory.get_connection()
		db.add(member)
		db.commit()
	
	def update(self, member):
		"""
		"""
		db = self.factory.get_connection()
		db.merge(member)
		db.commit()
		
	def delete(self, member):
		"""
		"""
		db = self.factory.get_connection()
		db.delete(member)
		db.commit()
	
