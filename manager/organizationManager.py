#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.organization import Organization
from lib.entity.organization import OrganizationRole
from lib.exceptions import DBError

class OrganizationManager(object):
	"""Implementation of the DAO pattern for the machine entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
	
	def find(self, value, find_by="id"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Organization)
		if find_by == "id":
			query = query.filter(Organization.id == value)
		elif find_by == "role":
			query = query.filter(Organization.role == value)
		elif find_by == "member":
			query = query.filter(Organization.member == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Organization" % find_by)
		return query.all()
	
	def find_all(self, order_by="id", rev=False, start=None, stop=None):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Organization)
		
		criterion = None
		if order_by == "id":
			criterion = Organization.id
		elif order_by == "role":
			criterion = Organization.role
		elif order_by == "member":
			criterion = Organization.member
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Oranization" % order_by)
		if rev:
			query = query.order_by(desc(criterion))
		else:
			query = query.order_by(criterion)
		if start and not stop:
			query = query.limit(start)
		elif start and stop:
			query = query.slice(start, stop)
		
		return query.all()
	
	def add(self, organization):
		"""
		"""
		db = self.factory.get_connection()
		db.add(organization)
		db.commit()
	
	def update(self, organization):
		"""
		"""
		db = self.factory.get_connection()
		db.merge(organization)
		db.commit()
		
	def delete(self, organization):
		"""
		"""
		db = self.factory.get_connection()
		db.delete(organization)
		db.commit()
		
class OrganizationRoleManager(object):
	"""
	"""
	
	def __init__(self, factory):
		self.factory = factory
		
	def find_all(self):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(OrganizationRole)
		 
		return [r.role for r in query.all()]