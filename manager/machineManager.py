#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.machine import Machine
from lib.exceptions import DBError

class MachineManager(object):
	"""Implementation of the DAO pattern for the machine entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
	
	def find(self, value, find_by="mac"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Machine)
		if find_by == "mac":
			query = query.filter(Machine.mac == value)
		elif  find_by == "owner":
			query = query.filter(Machine.owner == value)
		elif  find_by == "name":
			query = query.filter(Machine.name == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Machine" % find_by)
		return query.all()
		
	def add(self, machine):
		"""
		"""
		db = self.factory.get_connection()
		db.add(machine)
		db.commit()
	
	def update(self, machine):
		"""
		"""
		db = self.factory.get_connection()
		db.merge(machine)
		db.commit()
		
	def delete(self, machine):
		"""
		"""
		db = self.factory.get_connection()
		db.delete(machine)
		db.commit()