#!/bin/env python
#*-* codiing: utf-8 *-*

from lib.entity.building import Building
from lib.exceptions import DBError

class BuildingManager(object):
	"""Implementation of the DAO pattern for the Building entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
	
	def find(self, value):
		"""Retrieve a Building by his Name
		"""
		db = self.factory.get_connection()
		query = db.query(Building).filter(Building.name == value)
		return query.one()
	
	def find_all(self, start=None, stop=None):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Building)
		if start and not stop:
			query = query.limit(start)
		elif start and stop:
			query = query.slice(start, stop)
		return query.all()
