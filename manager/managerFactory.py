#!/bin/env python
#*-* codiing: utf-8 *-*

import lib.model

from lib.manager.buildingManager import BuildingManager
from lib.manager.switchManager import SwitchManager
from lib.manager.roomManager import RoomManager
from lib.manager.ipManager import IpManager
from lib.manager.machineManager import MachineManager
from lib.manager.memberManager import MemberManager
from lib.manager.paymentManager import PaymentManager
from lib.manager.paymentManager import PaymentTypeManager
from lib.manager.organizationManager import OrganizationManager
from lib.manager.organizationManager import OrganizationRoleManager

class ManagerFactory(object):
	"""Factory for all entity managers
	"""
	
	# Ensure there is only one instance of the factory running at a time
	# (Singleton design pattern)
	instance = None       
	def __new__(cls, *args, **kargs): 
		if cls.instance is None:
			cls.instance = object.__new__(cls, *args, **kargs)
		return cls.instance
	
	def __init__(self, db_session):
		"""Create an Entity Manager
		"""
		self.session = db_session
	
	def get_connection(self):
		return self.session
	
	def buildingManager(self):
		return BuildingManager(self)
	
	def switchManager(self):
		return SwitchManager(self)
		
	def roomManager(self):
		return RoomManager(self)
	
	def memberManager(self):
		return MemberManager(self)
	
	def machineManager(self):
		return MachineManager(self)
	
	def ipManager(self):
		return IpManager(self)
	
	def paymentManager(self):
		return PaymentManager(self)
	
	def paymentTypeManager(self):
		return PaymentTypeManager(self)
	
	def organizationManager(self):
		return OrganizationManager(self)
	
	def organizationRoleManager(self):
		return OrganizationRoleManager(self)