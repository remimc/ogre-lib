#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.room import Room
from lib.entity.member import Member
from lib.exceptions import DBError

class RoomManager(object):
	"""Implementation of the DAO pattern for the Room entity
	"""
	def __init__(self, factory):
		"""
		"""
		self.factory = factory
	
	def find_free(self):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Room)
		room_with_member = db.query(Member.room).filter(Member.room != None)
		
		return query.filter(~Room.name.in_(room_with_member)).all()
		
	def find(self, value, find_by="name"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Room)
		if find_by == "name":
			query = query.filter(Room.name == value)
		elif find_by == "location":
			query = query.filter(Room.location == value)
		elif find_by == "switch":
			query = query.filter(Room.switch == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Switch" % find_by)
		
		return query.all()
	
	def find_all(self, order_by="name", rev=False, start=None, stop=None):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Room)
		
		criterion = None
		if order_by == "name":
			criterion = Room.name
		elif order_by == "location":
			criterion = Room.location
		elif order_by == "switch":
			criterion = Room.switch
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Switch" % order_by)
		if rev:
			query = query.order_by(desc(criterion))
		else:
			query = query.order_by(criterion)
		if start and not stop:
			query = query.limit(start)
		elif start and stop:
			query = query.slice(start, stop)
		
		return query.all()
