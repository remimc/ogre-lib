#!/bin/env python
#*-* codiing: utf-8 *-*

from lib import utilities
from lib.entity.switch import Switch
from lib.exceptions import DBError

class SwitchManager(object):
	"""Implementation of the DAO pattern for the switch entity
	"""
	
	def __init__(self, factory):
		self.factory = factory
			
	def find(self, value, find_by="name"):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Switch)
		if find_by == "name":
			query = query.filter(Switch.name == value)
		elif find_by == "location":
			query = query.filter(Switch.name == value)
		elif find_by == "address":
			query = query.filter(Switch.name == value)
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Switch" % find_by)
		
		return query.all()
	
	def find_all(self, order_by="name", rev=False, start=None, stop=None):
		"""
		"""
		db = self.factory.get_connection()
		query = db.query(Switch)
		
		criterion = None
		if order_by == "name":
			criterion = Switch.name
		elif order_by == "location":
			criterion = Switch.location
		elif order_by == "address":
			criterion = Switch.address
		else:
			raise DBError(DBError.PROGRAMMING, "Field '%s' not found for Switch" % order_by)
		if rev:
			query = query.order_by(desc(criterion))
		else:
			query = query.order_by(criterion)
		if start and not stop:
			query = query.limit(start)
		elif start and stop:
			query = query.slice(start, stop)
		