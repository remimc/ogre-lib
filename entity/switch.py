#!/bin/env python
#*-* codiing: utf-8 *-*

from entity import Entity

class Switch(Entity):
	"""
	Represents a switch
	"""
	def __init__(self, name=None, location=None, address=None):
		self.name = name
		self.location = location
		self.address = address

