#!/bin/env python
#*-* codiing: utf-8 *-*

from entity import Entity

class Organization(Entity):
	"""
	Represents the Organization
	"""
	def __init__(self, id=None, role=None, member=None):
		self.id = id
		self.role = role
		self.member = member

class OrganizationRole(Entity):
	"""Represents the differents roles in the Organization 
	"""
	def __init__(self, role=None):
		self.role = role