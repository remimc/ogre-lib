#!/bin/env python
#*-* codiing: utf-8 *-*

from entity import Entity

class Member(Entity):
	"""
	Represents a member
	"""
	def __init__(self, id=None, firstName=None, lastName=None, email=None, newsOK=True, login=None, password=None, room=None, regFrom=None, regValid=False, networkUntil=None, blocked=None):
		self.id = id
		self.firstname= firstName
		self.lastname = lastName
		self.email = email
		self.newsok = newsOK
		self.login = login
		self.password = password
		self.room = room
		self.regfrom = regFrom
		self.regvalid = regValid
		self.networkuntil = networkUntil
		self.blocked = blocked
	
