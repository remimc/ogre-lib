#!/bin/env python
#*-* codiing: utf-8 *-*

from entity import Entity

class Machine(Entity):
	"""
	Represents a machine
	"""
	def __init__(self, mac=None, owner=None, name=None):
		self.mac = mac
		self.owner = owner
		self.name = name
