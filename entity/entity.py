#!/bin/env python
#*-* codiing: utf-8 *-*

class Entity(object):
	"""
	Base entity
	"""
	def __repr__(self):
		return "<%s%s>" % (self.__class__.__name__, vars(self))
	
	def __eq__(self, other):
		if not isinstance(other, self.__class__):
			return False
		for prop, val in vars(other).iteritems():
			if getattr(self, prop) != val:
				return False
		return True
			