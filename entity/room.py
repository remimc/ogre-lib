#!/bin/env python
#*-* codiing: utf-8 *-*

from entity import Entity

class Room(Entity):
	"""
	Represents a room
	"""
	def __init__(self, name=None, floor=None, location=None, switch=None, port=None):
		self.name = name
		self.floor = floor
		self.location = location
		self.switch = switch
		self.port = port

