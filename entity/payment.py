#!/bin/env python
#*-* codiing: utf-8 *-*

from entity import Entity

class Payment(Entity):
	"""
	Represents a transaction
	"""
	def __init__(self, id=None, date=None, amount=None, type=None, comment=None, member=None):
		self.id = id
		self.date = date
		self.amount = amount
		self.type = type
		self.comment = comment
		self.member = member

class PaymentType(Entity):
	"""Represents the types of transcations
	"""
	def __init__(self, type=None):
		self.type = type