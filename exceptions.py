#!/bin/env python
#*-* codiing: utf-8 *-*

class DBError(Exception):
	PROGRAMMING	= 1
	OPERATIONAL	= 2
	INTEGRITY	= 3
	def __init__(self, err_type, message):
		self.message = message
		self.err_type = err_type
	def __str__(self):
		if self.err_type == 1:
			error_repr = "DBError (Programming) %s" % self.message
		elif self.err_type == 2:
			error_repr = "DBError (Integrity) %s" % self.message
		elif self.err_type == 3:
			error_repr = "DBError (Operational) %s" % self.message
		return error_repr
	
class BuildingError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "BuildingError: %s" % self.message

class SwitchError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "SwitchError: %s" % self.message

class RoomError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "RoomError: %s" % self.message
	
class MemberError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "MemberError: %s" % self.message

class MachineError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "MachineError: %s" % self.message
	
class IpError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "IpError: %s" % self.message

class OrganizationError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "OrganizationError: %s" % self.message
	
class PaymentError(Exception):
	def __init__(self, message):
		self.message = message
	
	def __str__(self):
		return "PaymentError: %s" % self.message
